mod julia;

use sfml::graphics::*;
use nalgebra as na;
use na::Complex;

use std::path::Path;
use std::fs;

const IMAGE_DIMS: (u32, u32) = (1280, 720);

fn main() {
    create_or_clear_output_location(&Path::new("."));

    let mut jul = julia::Julia::new(
        Complex::new(-0.348827, 0.607167),
        Complex::new(IMAGE_DIMS.0 as f64/2.0, IMAGE_DIMS.1 as f64/2.0),
        1.5,
        100
    );

    let image = jul.generate_image(IMAGE_DIMS);
}

fn create_or_clear_output_location(path: &Path) {
    if let Err(_) = fs::create_dir(path) {
        // If dir already exists, clear it
        fs::remove_dir_all(path).unwrap();
        fs::create_dir(path).unwrap();
    }

    fs::create_dir(path.join("frames")).unwrap()
}
