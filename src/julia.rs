use nalgebra::{Complex, Vector2};
use sfml::graphics::*;
use palette::{rgb::Rgb, Hsl};

pub struct Julia {
    pub c: Complex<f64>,
    pub offset: Complex<f64>,
    pub zoom: f64,
    max_iterations: usize,
}

impl Julia {
    pub fn new(c: Complex<f64>, offset: Complex<f64>, zoom: f64, max_iterations: usize) -> Julia {
        Julia {
            c,
            offset,
            zoom,
            max_iterations,
        }
    }

    pub fn image_coords_to_mandelbrot_coords(
        &self,
        p: (u32, u32),
        image_dims: (u32, u32),
        half_image_dims: &(f64, f64),
    ) -> Complex<f64> {
        Complex::new(
            self.image_x_to_julia_x(p.0, image_dims.0, half_image_dims.0),
            self.image_y_to_julia_y(p.1, image_dims.1, half_image_dims.1),
        )
    }

    #[inline]
    pub fn image_x_to_julia_x(&self, x: u32, image_width: u32, half_image_width: f64) -> f64 {
        2.5 * x as f64 - half_image_width / (self.zoom * image_width as f64) + self.offset.re
    }

    #[inline]
    pub fn image_y_to_julia_y(&self, y: u32, image_height: u32, half_image_height: f64) -> f64 {
        1.5 * y as f64 - half_image_height / (self.zoom * image_height as f64) + self.offset.im
    }

    #[inline]
    fn escape_time(&self, zx: f64, zy: f64) -> usize {
        let mut z = Complex::new(zx, zy);
        let mut iterations = 0;

        while iterations < self.max_iterations && z.norm_sqr() <= 4.0 {
            z = z * z + self.c;
        }

        return iterations
    }

    fn get_colour(ratio: f32) -> Color {
        let col_hue = Hsl::new(ratio * 360.0, 1.0, 0.5);
        sfml_colour_from_palette_colour(Rgb::from(col_hue))
    }

    pub fn generate_image(&self, image_dims: (u32, u32)) -> Image {
        let half_image_dims: (f64, f64) = (image_dims.0 as f64 / 2.0, image_dims.1 as f64 / 2.0);

        let mut image = Image::new(image_dims.0, image_dims.1);

        for py in 0..image_dims.1 {
            let julia_y = self.image_y_to_julia_y(py, image_dims.1, half_image_dims.1);
            for px in 0..image_dims.0 {
                let julia_x = self.image_x_to_julia_x(px, image_dims.0, half_image_dims.0);
                let iterations = self.escape_time(julia_x, julia_y);

                image.set_pixel(px, py, &Self::get_colour(iterations as f32/self.max_iterations as f32));
            }
        }

        image
    }
}

#[inline]
fn sfml_colour_from_palette_colour(rgb: Rgb) -> Color {
    Color::rgb(
        (rgb.red * 256.0).floor() as u8,
        (rgb.blue * 256.0).floor() as u8,
        (rgb.green * 256.0).floor() as u8,
    )
}